/*
    Modulo: Pantalla.cpp

    Author: Jose Luis Perez Barrales
    Modified: Francisco Ochando

    Contiene los metodos de la clase

        void Screen::show( byte numero )    Permite dibujar diferentes pantallas a partir del parametro
        void Screen::PintaLCD( void )       Vuelca el contenido de la clase ( buffer ) en el LCD

    Para mas detalle sobre la clase Screen consultar Reespirator.h  

 */
 
#define PANTALLA_MAIN                          0
#define PANTALLA_REPOSO                        2
#define PANTALLA_CONFIG                        3
#define PANTALLA_REPOSO_CONFIG                 4
#define PANTALLADATOS_FRECUENCIA              11
#define PANTALLADATOS_PRESIONPICO             12
#define PANTALLADATOS_PRESIONPEEP             13
#define PANTALLADATOS_VOLUMEN_ALTO             4
#define PANTALLADATOS_VOLUMEN_BAJO             5
#define PANTALLADATOS_FRECUENCIA_ALTO          6
#define PANTALLADATOS_FRECUENCIA_BAJO          7
#define PANTALLADATOS_PRESIONPICO_ALTO         8
#define PANTALLADATOS_PRESIONPICO_BAJO         9
#define PANTALLADATOS_PRESIONPEEP_ALTO        10
#define PANTALLADATOS_PRESIONPEEP_BAJO        11
#define PANTALLADATOS_BATERIA_ALTO            12
#define PANTALLADATOS_SALIR                   13

#include  "HID.h"


void Screen::Setup()
{

    LCD.init();
    LCD.backlight();

    // Se coloca en col 0 fila 0 y muestra el logo
    LCD.setCursor ( 0, 0 );
    LCD.print("Respirator 23");
    delay(500);       // Espera de Bienvenida

  lcd2004.pantalla.show( PANTALLA_REPOSO );  
}


/*
 
    Métodos privados
    Metodos de escritura en Pantalla

    PintaLCD(void)   Pinta completa
    PintaRow( byte row, String cadena )   Pinta una fila
    PintaGroup( byte row, byte col, String cadena )    Pinta un grupo de caracteres

 */
 
void Screen::PintaLCD( void )
{
  
  LCD.setCursor ( 0, 0 );
  LCD.print(linea1);
  LCD.setCursor ( 0, 1 );
  LCD.print(linea2);
  LCD.setCursor ( 0, 2 );
  LCD.print(linea3);
  LCD.setCursor ( 0, 3 );
  LCD.print(linea4);
  
}

void Screen::PintaRow( byte row, String cadena )
{
  
  LCD.setCursor ( 0, row );
  LCD.print(cadena);

}

void Screen::PintaGroup( byte row, byte col, String cadena )
{
  
  LCD.setCursor ( col, row );
  LCD.print(cadena);

}


void Screen::Update( byte numero )
{
    switch( numero )
    {
        case PANTALLA_REPOSO:
            linea1 = "Principal        <1>";
            linea2 = " Frecuencia         ";
            linea3 = " P. Pico            ";
            linea4 = " P. PEEP            "; 
        break;
        
        case PANTALLA_CONFIG:
            linea1 = ">Principal       <1>";
            linea2 = " Frecuencia         ";
            linea3 = " P. Pico            ";
            linea4 = " P. PEEP            "; 
        break;

        case PANTALLADATOS_FRECUENCIA:
            linea1 = " Frecuencia    <1.1>";
            linea2 = "                    ";
            linea3 = " Rpm:               ";
            linea4 = "     <Pulsa encoder>";
        break;
    
        case PANTALLADATOS_PRESIONPICO:
            linea1 = "P. Pico        <1.2>";
            linea2 = "                    ";
            linea3 = " Presion:           ";
            linea4 = "     <Pulsa encoder>";
        break;
    
        case PANTALLADATOS_PRESIONPEEP:
            linea1 = "P. PEEP        <1.3>";
            linea2 = "                    ";
            linea3 = " Presion:           ";
            linea4 = "     <Pulsa encoder>";      
        break;

        case PANTALLADATOS_VOLUMEN_ALTO:
            linea1 = "Vol. Alto      <4.1>";
            linea2 = "                    ";
            linea3 = " Volumen:           ";
            linea4 = "     <Pulsa encoder>";      
        break;

        case PANTALLADATOS_VOLUMEN_BAJO:
            linea1 = "Vol. Bajo      <4.2>";
            linea2 = "                    ";
            linea3 = " Volumen:           ";
            linea4 = "     <Pulsa encoder>";
        break;

        case PANTALLADATOS_FRECUENCIA_ALTO:
            linea1 = "Frec. Alto     <4.3>";
            linea2 = "                    ";
            linea3 = " Volumen:           ";
            linea4 = "     <Pulsa encoder>";
        break;

        case PANTALLADATOS_FRECUENCIA_BAJO:
            linea1 = "Frec. Bajo     <4.4>";
            linea2 = "                    ";
            linea3 = " Volumen:           ";
            linea4 = "     <Pulsa encoder>";
        break;

        case PANTALLADATOS_PRESIONPICO_ALTO:
            linea1 = "P. Pico        <4.5>";
            linea2 = "                    ";
            linea3 = " Presion:           ";
            linea4 = "     <Pulsa encoder>";
        break;

        case PANTALLADATOS_PRESIONPICO_BAJO:
            linea1 = "P. Pico        <4.6>";
            linea2 = "                    ";
            linea3 = " Presion:           ";
            linea4 = "     <Pulsa encoder>";      
        break;

        case PANTALLADATOS_PRESIONPEEP_ALTO:
            linea1 = "P. PEEP        <4.7>";
            linea2 = "                    ";
            linea3 = " Presion:           ";
            linea4 = "     <Pulsa encoder>";      
        break;

        case PANTALLADATOS_PRESIONPEEP_BAJO:
            linea1 = "P. PEEP        <4.8>";
            linea2 = "                    ";
            linea3 = " Presion:           ";
            linea4 = "     <Pulsa encoder>";      
        break;

        case PANTALLADATOS_BATERIA_ALTO:
            linea1 = "Bateria        <4.9>";
            linea2 = "                    ";
            linea3 = " Bateria:           ";
            linea4 = "     <Pulsa encoder>";      
        break;
        
        default:
            Serial.print("Pantalla no existe.");
        break;
    }
    PintaLCD();
}


/*
 
    Métodos publicos
    Metodos de escritura en Pantalla

    Show( int indice )   Actualiza Pantalla por numero

 */

void Screen::Show( int indice )
{

  lcd2004.pantalla.Update( indice );  

}