/*

    Class HID.h
    
    Class header 
    
    Manages Human Interface Device
    
*/



enum State {
    SCREEN_INICIO = 1,
    SCREEN_REPOSO = 2,
    SCREEN_CONFIG = 3,  
    SCREEN_REPOSO_CONFIG = 4,
    SCREEN_START = 5,
    SCREEN_EDIT = 6,
    SCREEN_REVISION_CONSIGNA = 7,
    SCREEN_CONSIGNAS = 8,
    SCREEN_EDIT_TRIGGER = 9,
    SCREEN_RECRUIT = 10,
    SCREEN_EDIT_RECRUIT = 11,
    SCREEN_CONFIRMA_REPOSO = 12,
    SCREEN_CONFIRMA_OFF = 13
};


class HID
{
    
    // Clase: hid.cpp
     
    // Contiene la clase que maneja el interface humano y la máquina de estados
    // Recibe las entradas de los botones y envía las llamadas a la pantalla.
    
    // Contiene los siguientes metodos de la clase
    
    private:
        byte hidestadoanterior;
        byte hidestadoactual;

        // Metodos
        byte HID::Update( byte estado )
        byte HID::read( byte entrada, byte estado )

    public:
 	
        // Metodos
        bool readButton( byte boton );
        bool readAlarm();
        void readAlarm( byte codigo );
        byte readHID( void );
        byte getEstado( void );
        byte Update( byte estado );
        byte HID::getInput( void );
        void writeValues( void );
        void writeAlarms( void );
		
}


class Botonera
{
  private:
  
    static unsigned char bufferbotonera[BUFFER_BOTONERA];   //    Almacena el estado instantaneo de las entradas
    static unsigned char indicebuffer;                      //    Indice actual de muestras
    static unsigned char estado , estadoanterior;

        
    RotaryEncoder encoderAlarmas;  
      
    //    Metodos
  public:
  
    void LeeEntradas( void );                               //    Muestrea las entradas y las almacena en buffer[]
    byte LeeTeclado( void );                                //    Muestrea las entradas y las almacena en buffer[]
    int Pulsaciones( void );  
      
    byte Teclado( void );                // Devuelve el Estado, boton pulsado
    byte PulsadoEfectivo();
    int EstadoBotonera( void );                                    //     Obtiene el estado del sistema

    int ObtenEstadoPorlotes( void );

    void Reset( void );                 // Reset de bufer de valores
    void Rst( void );                   // Reset devalores
    
    Botonera(){ } 
    
};