/*
    Modulo: Botonera.cpp
    
    Author: Jose Luis Perez Barrales
    Modified: Francisco Ochando
     
    Description: Clase Teclado. Contiene los metodos de la clase

    Private:
        void LeeEntradas( void );       //    Muestrea las entradas y las almacena en buffer[]
        int EstadoBotonera( void );     //    Obtiene el estado del sistema
        
    Public:
        int getEstado( void );          //    Retorna el estado inmediato del buffer de Teclado 
                                        //    Calculando el estado con los muestreos actuales
        byte Debounced();               //    Devuelve la tecla pulsada mediante debouncing
        
 */
#include  "HID.h"

byte buton = 0;
int pulsado = 0;
int time_limit = 1000;
byte ticks_reloj_min = 4;
byte ticks_reloj_max = 6;

void Botonera::LeeEntradas( void )
{
  static char pasoprograma;
  
  if ( !digitalRead(SWITCH_RPM) )
    bufferbotonera[indicebuffer] += SWITCH_RPM_PULSADO;
    
  //     Switch  Presion Pico
  if ( !digitalRead(SWITCH_PICO) )
    bufferbotonera[indicebuffer]  += SWITCH_PICO_PULSADO;

  //    Switch  Presion PEEP
  if ( !digitalRead(SWITCH_PEEP) )
    bufferbotonera[indicebuffer]  += SWITCH_PEEP_PULSADO;
         
  //    Switch  ALARMAS
  if ( !digitalRead(SWITCH_ALARMAS) )
    bufferbotonera[indicebuffer]  += SWITCH_ALARMAS_PULSADO;  

  //  EncoderS
  bufferbotonera[indicebuffer]  += encoderAlarmas.LeeEncoder();
   
  //    Actualizamos el indice
  indicebuffer++;
  if( indicebuffer >= BUFFER_BOTONERA )
  {
    estadoanterior = EstadoBotonera();
    indicebuffer = 0 ;
  }  
}

/*

    Modulo LeeTeclado
    
    Author: Francisco Ochando
    
    Description: Modulo simplista que devuelve el ultimo boton pulsado, en 
    terminos infinitésimos. El teclado no admite combinaciones, por tanto, no es 
    necesario un tratamiento complejo de las teclas pulsadas. 
    No sabemos como afectan las microinterrupciones.
    
 */

byte Botonera::LeeTeclado( void )
{
  buton = 0;
  
  if ( !digitalRead(SWITCH_RPM) ) {
    buton = SWITCH_RPM_PULSADO;
    time_pulsado++;
  }
    
  //     Switch  Presion Pico
  if ( !digitalRead(SWITCH_PICO) ) {
    buton = SWITCH_PICO_PULSADO;
    time_pulsado++;   
  }
  
  //    Switch  Presion PEEP
  if ( !digitalRead(SWITCH_PEEP) ) {
    buton = SWITCH_PEEP_PULSADO;
    time_pulsado++;
  }
             
  //    Switch  ALARMAS
  if ( !digitalRead(SWITCH_ALARMAS) ) {
    buton = SWITCH_ALARMAS_PULSADO;  
    time_pulsado++;
  }
   
  return buton;
}

int Botonera::Pulsaciones( void )
{
  return time_pulsado;
  buton = 0;
  time_pulsado = 0;
}

/*

    Modulo Debounced
    
    Author: Francisco Ochando
    
    Modulo que determina si la pulsacion del boto almacenada es efectiva.
    Comprueba que se ha ejecutado durante un numero de ciclos de reloj
    Devuelve el boton y reinicia los valores.
    
 */

byte Botonera::Debounced()
{
  if (LeeTeclado() != 0 && pulsado > 3) {
    return LeeTeclado();
  } else
  {
    return 0;
  }
  buton = 0;
  pulsado = 0;
}

/*  
 
    Metodo:     int Botonera::Estado( void )

    Description: Devuelve el estado del Teclado
    
*/

int Botonera::getEstado( void )
{
  return( EstadoBotonera() );
}  

