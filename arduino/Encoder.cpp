/*
    Modulo Encoder

    Maneja el Encoder Rotativo

    Metodos:
        int RotaryEncoder::read( void )
        int RotaryEncoder::LeeSwitch( void )
   
  
 */
#include "HID.h"


#define INC 5       //  Valor de incremento del encoder en cada paso
#define NULL 0

int valorinicio;    //  El original al pulsar algun switch el operario
int valoractual;    //  El que esta cambiando el operario right now ! 
int vminimo;        
int vmaximo;


/*
      Metodo:LeeEncoder()
        Lee el estado del encoder y devuelve el estado del sistema

            encoder        { ENCODER_NULL , ENCODER_GIRODCHA , ENCODER_GIROIZDA }

      
 */

int Encoder::read( void )
{
  static int aLastState;
  static int aState;

  int encoder = NULL;
  
  aLastState = digitalRead(outputA);
    delayMicroseconds(5);                                 //    Esto no deberia parar nada

  aState = digitalRead(outputA);  
  
  if (aState != aLastState)
   {
      if (digitalRead(outputB) != aState) 
       {
          encoderAngulo+=INC;
          encoder = ENCODER_GIRODCHA_PULSADO;
      }
      else
      {
          encoderAngulo-=INC;
          encoder = ENCODER_GIROIZDA_PULSADO;
      }
   } 
  aLastState = aState; // Guardamos el ultimo valor
  return (encoder);
}


/*
      Metodo:Switch()
        Lee el estado del encoder y devuelve un valor segun el suceso, el cual puede ser

            pulsador --->  {  SWITCHON  , SWITCHOFF }
            
      Notas: Requiere tecnicas de debouncing
      Obviamente un debouncing por hardware ( via condensador por ejemplo ) complementaria este Metodo
 */
 
int Encoder::LeeSwitch( void )
{
  //    Control del estado del switch
  if ( !digitalRead( SWITCH_ALARMAS ) )
    return SWITCH_ALARMAS_PULSADO;
  else
    return SWITCH_OFF;
}

/*
    Module: doEncode()
    Description: Lectura del Encoder y cuenta
*/

void doEncode()
{
   if (micros() > timeCounter + timeThreshold)
   {
      if (digitalRead(channelPinA) == digitalRead(channelPinB))
      {
         IsCW = true;
         if (ISRCounter + 1 <= maxSteps) ISRCounter++;
      }
      else
      {
         IsCW = false;
         if (ISRCounter - 1 > 0) ISRCounter--;
      }
      timeCounter = micros();
   }
}

/*
    Module: readISR()
    Description: Lectura del Encoder disparada por interrupcion
*/

int Encoder::readISR()
{
   if (counter != ISRCounter)
   {
        counter = ISRCounter;
        return counter;
   } else {
        return 0;
   }
}

/*
Digital Debounce Filter

The digital filter is made up of a single 16 bit integer variable into which you 
shift the current state of the input pin: 

state=(state<<1) | digitalRead(CLK_PIN) | 0xe000;

Each time round the loop a new bit is shifted left (at bit 0). The "or" action 
with 0xe000 defines the number of iterations i.e. the top 3 bits are blocked off 
leaving the rest as useful inputs. The idea is that you test for the state 0xf000
which can only occur if there was a sequence of 1 0000 0000 0000 inputs meaning 
that the signal has been stable for 12 iterations around the loop i.e. 
not bouncing around.

https://www.best-microcontroller-projects.com/rotary-encoder.html#Digital_Debounce_Filter

*/

int EncodeFiltered() 
{
    static uint16_t state = 0, counter = 0;

    delayMicroseconds(100); // Simulate doing somehing else as well.

    state=(state<<1) | digitalRead(CLK_PIN) | 0xe000;

    if (state==0xf000){
        state=0x0000;
        if(digitalRead(DT_PIN))
            counter++;
        else
            counter--;
    }
    return counter;
}

