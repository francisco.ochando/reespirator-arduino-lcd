/*

    Author: Francisco Ochando
    Modulo: HID.cpp
     
    Contiene los metodos de la clase

    Private:
        byte InputEstado()
        void UpdateHID( void )

    Public:
        void Update( void )
        byte get( void )      

 */

#include  "HID.h"
extern Hid lcd2004;


#define ESTADO_ALARMAS                   128
#define ESTADO_ALARMAS                    64
#define ESTADO_ENCODER_GIROIZDA_PULSADO   32
#define ESTADO_ENCODER_GIRODCHA_PULSADO   16
#define ESTADO_ENCODER_CENTRO_PULSADO      0
#define ESTADO_SWITCH_RPM_PULSADO          8
#define ESTADO_SWITCH_PICO_PULSADO         4
#define ESTADO_SWITCH_PEEP_PULSADO         2
#define ESTADO_SWITCH_ALARMAS_PULSADO      1
#define ESTADO_SWITCH_OFF                  0

/*

    Los estados de la máquina según documento de la máquina de estados
    Los estados intermedios de configuracion de parametros están definidos en los siguientes intervalos:
    Configuracion inicial ( 31 .. 39 )
    Edicion simpe de parámetros ( 61 .. 69 )
    
 */
 
enum EstadosHID {
  MAQUINA_REPOSO = 2,
  MAQUINA_CONFIG = 3,
  MAQUINA_REPOSO_CONFIG = 4,
  MAQUINA_START = 5,
  MAQUINA_REVISION_CONSIGNA = 7,
  MAQUINA_CONSIGNAS = 8,
  MAQUINA_EDIT_TRIGGER = 9,
  MAQUINA_RECRUIT = 10,
  MAQUINA_EDIT_RECRUIT = 11,
  MAQUINA_CONFIRMA_REPOSO = 12,
  MAQUINA_CONFIRMA_OFF = 13  
};


/*

     Metodo:   InputEstado
     El Método privado comprueba el estado de los botones y cambia el estado de la máquina en función de los botones
     El método comprueba el estado de las variables y actualiza el estado de la máquina

 */


byte HID::InputTeclado( void )
{
  return lcd2004.miBotonera.Estado();          // Obtiene el estado del teclado  
}   


/*

     Metodo:   UpdateHID
     El Método recibe UpdateHID comprueba los botones (inuts) y cambia el estado del HID
     El método tambien recibe el estado de las variables y muestra los valores
     
     Si disparamos el método mediante el timer desde el Loop principal, el método comprueba si se ha pulsado un boton 
     y cambia el estado de la máquina. El estado de la máquina marca el estado de la pantalla.

     Este proceso sólo necesita la relacion  ( No Estado de la máquina ) ---> ( No Pantalla en Pantalla.cpp )

 */
    
void HID::UpdateHID( void )
{
    int variables[3] = {0, 0, 0}; 
    byte botons = InputTeclado(); 
    // Lee Estado de la Botonera (metodo privado), lo llama directamente
    // La gestion de dos botones a la vez la realiza la clase teclado
    if( hidestadoanterior == 0 ) {
        switch( botons )
        {
        case ESTADO_SWITCH_PICO_PULSADO:
            hidestadoactual = 3;
        break;
        case ESTADO_SWITCH_PEEP_PULSADO:
            hidestadoactual = 3;
        break;
        case ESTADO_SWITCH_RPM_PULSADO:
            hidestadoactual = 4;
        break;
        case ESTADO_ENCODER_CENTRO_PULSADO:
            hidestadoactual = 4;
        break;
        }
    }
    
    else if( hidestadoanterior == MAQUINA_CONFIG ) { // Config inicial. Si se pulsa encoder marca submenu Frecuencia
        switch( botons )
        {
        case ESTADO_SWITCH_PICO_PULSADO:
            hidestadoactual = 4;
        break;
        case ESTADO_SWITCH_PEEP_PULSADO:
            hidestadoactual = 3;
        break;
        case ESTADO_SWITCH_RPM_PULSADO:
            hidestadoactual = 3;
        break;
        case ESTADO_ENCODER_CENTRO_PULSADO:
            hidestadoactual = 31;
        break;
        case ESTADO_ENCODER_GIRODCHA_PULSADO:
            hidestadoactual = 3;
        break;
        case ESTADO_ENCODER_GIROIZDA_PULSADO:
            hidestadoactual = 3;
        break;
        }    
    }

    else if( hidestadoanterior == MAQUINA_REPOSO_CONFIG ) { // Reposo. Espera solicitud Config/Marcha/Apagado
        switch( botons )
        {
        case ESTADO_SWITCH_PICO_PULSADO:
            hidestadoactual = 3;
        break;
        case ESTADO_SWITCH_PEEP_PULSADO:
            hidestadoactual = 5;
        break;
        case ESTADO_SWITCH_RPM_PULSADO:
            hidestadoactual = 4;
        break;
        case ESTADO_ENCODER_CENTRO_PULSADO:
            hidestadoactual = 13;
        break;
        }    
    }
    
    else if( hidestadoanterior == MAQUINA_START ) { // Estado en marcha. Funcionamiento normal
        switch( botons )
        {
        case ESTADO_SWITCH_PICO_PULSADO:
            hidestadoactual = 61;
        break;
        case ESTADO_SWITCH_PEEP_PULSADO:
            hidestadoactual = 62;
        break;
        case ESTADO_SWITCH_RPM_PULSADO:
            hidestadoactual = 63;
        break;
        case ESTADO_ENCODER_CENTRO_PULSADO:
            hidestadoactual = 8;
        break;
        }    
    }
    
    else if( hidestadoanterior == 61 ) { // Edicion simple PEAK
        switch( botons )
        {
        case ESTADO_SWITCH_PICO_PULSADO:
            hidestadoactual = 5;
        break;
        case ESTADO_SWITCH_PEEP_PULSADO:
            hidestadoactual = 61;
        break;
        case ESTADO_SWITCH_RPM_PULSADO:
            hidestadoactual = 5;
        break;
        case ESTADO_ENCODER_CENTRO_PULSADO:
            hidestadoactual = 7;
        break;
        }    
    }
    
    else if( hidestadoanterior == 62 ) { // Edicion simple PEEP
    switch( botons )
    {
      case ESTADO_SWITCH_PICO_PULSADO:
        hidestadoactual = 5;
      break;
      case ESTADO_SWITCH_PEEP_PULSADO:
        hidestadoactual = 62;
      break;
      case ESTADO_SWITCH_RPM_PULSADO:
        hidestadoactual = 5;
      break;
      case ESTADO_ENCODER_CENTRO_PULSADO:
        hidestadoactual = 7;
      break;
    }    
  }
  
  else if( hidestadoanterior == 63 ) { // Edicion simple FREQ
    switch( botons )
    {
      case ESTADO_SWITCH_PICO_PULSADO:
        hidestadoactual = 5;
      break;
      case ESTADO_SWITCH_PEEP_PULSADO:
        hidestadoactual = 63;
      break;
      case ESTADO_SWITCH_RPM_PULSADO:
        hidestadoactual = 5;
      break;
      case ESTADO_ENCODER_CENTRO_PULSADO:
        hidestadoactual = 7;
      break;
    }    
  }
  else if( hidestadoanterior == MAQUINA_REVISION_CONSIGNA ) { // Revision
    switch( botons )
    {
      case ESTADO_SWITCH_PICO_PULSADO:
        hidestadoactual = 5;
      break;
      case ESTADO_SWITCH_PEEP_PULSADO:
        hidestadoactual = 5;
      break;
      case ESTADO_SWITCH_RPM_PULSADO:
        hidestadoactual = 5;
      break;
      case ESTADO_ENCODER_CENTRO_PULSADO:
        hidestadoactual = 5;
      break;
    }    
  }
  else if( hidestadoanterior == MAQUINA_CONSIGNAS ) { // Consignas
    switch( botons )
    {
      case ESTADO_SWITCH_PICO_PULSADO:
        hidestadoactual = 10;
      break;
      case ESTADO_SWITCH_PEEP_PULSADO:
        hidestadoactual = 10;
      break;
      case ESTADO_SWITCH_RPM_PULSADO:
        hidestadoactual = 9;
      break;
      case ESTADO_ENCODER_CENTRO_PULSADO:
        hidestadoactual = 12;
      break;
    }    
  }
  
  else if( hidestadoanterior == MAQUINA_EDIT_TRIGGER ) { // Trigger edit. Estado 9
    switch( botons )
    {
      case ESTADO_SWITCH_PICO_PULSADO:
        hidestadoactual = 9;
      break;
      case ESTADO_SWITCH_PEEP_PULSADO:
        hidestadoactual = 5;
      break;
      case ESTADO_SWITCH_RPM_PULSADO:
        hidestadoactual = 5;
      break;
      case ESTADO_ENCODER_CENTRO_PULSADO:
        hidestadoactual = 5;
      break;
    }    
  }
  else if( hidestadoanterior == MAQUINA_RECRUIT ) { // RECRUIT. Estado 10
    switch( botons )
    {
      case ESTADO_SWITCH_PICO_PULSADO:
        hidestadoactual = 10;
      break;
      case ESTADO_SWITCH_PEEP_PULSADO:
        hidestadoactual = 11;
      break;
      case ESTADO_SWITCH_RPM_PULSADO:
        hidestadoactual = 11;
      break;
      case ESTADO_ENCODER_CENTRO_PULSADO:
        hidestadoactual = 5;
      break;
    }    
  }
  else if( hidestadoanterior == MAQUINA_EDIT_RECRUIT ) { // RECRUIT edit. Estado 11
    switch( botons )
    {
      case ESTADO_SWITCH_PICO_PULSADO:
        hidestadoactual = 5;
      break;
      case ESTADO_SWITCH_PEEP_PULSADO:
        hidestadoactual = 5;
      break;
      case ESTADO_SWITCH_RPM_PULSADO:
        hidestadoactual = 5;
      break;
      case ESTADO_ENCODER_CENTRO_PULSADO:
        hidestadoactual = 5;
      break;
    }    
  }
  
  else if( hidestadoanterior == MAQUINA_CONFIRMA_REPOSO ) { // Confirm Reposo. Estado 12
    switch( botons )
    {
      case ESTADO_SWITCH_PICO_PULSADO:
        hidestadoactual = 5;
      break;
      case ESTADO_SWITCH_PEEP_PULSADO:
        hidestadoactual = 5;
      break;
      case ESTADO_SWITCH_RPM_PULSADO:
        hidestadoactual = 5;
      break;
      case ESTADO_ENCODER_CENTRO_PULSADO:
        hidestadoactual = 4;
      break;
    }    
  }
  
  else if( hidestadoanterior == MAQUINA_CONFIRMA_OFF ) { // Confirm Apagado
    switch( botons )
    {
      case ESTADO_SWITCH_PICO_PULSADO:
        hidestadoactual = 5;
      break;
      case ESTADO_SWITCH_PEEP_PULSADO:
        hidestadoactual = 5;
      break;
      case ESTADO_SWITCH_RPM_PULSADO:
        hidestadoactual = 5;
      break;
      case ESTADO_ENCODER_CENTRO_PULSADO:
        hidestadoactual = 99;         // Estado de apagado de la máquina
      break;
    }    
  }
  
  else if( hidestadoanterior == 31 ) { // Marca Submenu Frecuencia
    switch( botons )
    {
      case ESTADO_SWITCH_PICO_PULSADO:
        hidestadoactual = 31;
      break;
      case ESTADO_SWITCH_PEEP_PULSADO:
        hidestadoactual = 31;
      break;
      case ESTADO_SWITCH_RPM_PULSADO:
        hidestadoactual = 31;
      case ESTADO_ENCODER_GIRODCHA_PULSADO:
        hidestadoactual = 32;
      break;
      case ESTADO_ENCODER_GIROIZDA_PULSADO:
        hidestadoactual = 34;
      break;
      case ESTADO_ENCODER_CENTRO_PULSADO:   // Entra edicion Frecuencia 
        hidestadoactual = 131;              // Pantalla de edicion.
      break;
    }    
  }

  else if( hidestadoanterior == 32 ) { // Marca Submenu PEAK
    switch( botons )
    {
      case ESTADO_SWITCH_PICO_PULSADO:
        hidestadoactual = 32;
      break;
      case ESTADO_SWITCH_PEEP_PULSADO:
        hidestadoactual = 32;
      break;
      case ESTADO_SWITCH_RPM_PULSADO:
        hidestadoactual = 32;
      case ESTADO_ENCODER_GIRODCHA_PULSADO:
        hidestadoactual = 33;
      break;
      case ESTADO_ENCODER_GIROIZDA_PULSADO:
        hidestadoactual = 31;
      break;
      case ESTADO_ENCODER_CENTRO_PULSADO:   // Entra en edicion PEAK
        hidestadoactual = 132;
      break;
    }    
  }

  else if( hidestadoanterior == 33 ) { // Marca submenu PEEP
    switch( botons )
    {
      case ESTADO_SWITCH_PICO_PULSADO:
        hidestadoactual = 33;
      break;
      case ESTADO_SWITCH_PEEP_PULSADO:
        hidestadoactual = 33;
      break;
      case ESTADO_SWITCH_RPM_PULSADO:
        hidestadoactual = 33;
      case ESTADO_ENCODER_GIRODCHA_PULSADO:
        hidestadoactual = 34;
      break;
      case ESTADO_ENCODER_GIROIZDA_PULSADO:
        hidestadoactual = 32;
      break;
      case ESTADO_ENCODER_CENTRO_PULSADO:   // Entra edicion PEEP
        hidestadoactual = 133;
      break;
    }    
  }

  else if( hidestadoanterior == 34 ) { // Marca submenu 
    switch( botons )
    {
      case ESTADO_SWITCH_PICO_PULSADO:
        hidestadoactual = 33;
      break;
      case ESTADO_SWITCH_PEEP_PULSADO:
        hidestadoactual = 33;
      break;
      case ESTADO_SWITCH_RPM_PULSADO:
        hidestadoactual = 33;
      case ESTADO_ENCODER_GIRODCHA_PULSADO:
        hidestadoactual = 34;
      break;
      case ESTADO_ENCODER_GIROIZDA_PULSADO:
        hidestadoactual = 32;
      break;
      case ESTADO_ENCODER_CENTRO_PULSADO:
        hidestadoactual = 133;
      break;
    }    
  }
  else
  {
    hidestadoactual = hidestadoanterior; // El estado no varia
  }
  hidestadoanterior = hidestadoactual;  

}


byte HID::getEstado( void )
{

  return hidestadoactual;       // Obtiene el estado del sistema.  

}

void HID::Update( byte estado )
{

  HID.UpdateHID( estado );       // Actualiza el estado del sistema. Se llama para actualizar el estado de la pantalla  

}
